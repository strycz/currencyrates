package com.relink.revolutcurrencies.currencyList

import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.RecyclerView
import com.relink.revolutcurrencies.R
import com.relink.revolutcurrencies.utils.inflate
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.currency_row.view.*
import java.util.*
import kotlin.collections.ArrayList

class CurrencyRecyclerAdapter(private val currencyKeys: ArrayList<String>,
                              private val currencyValues: ArrayList<Float>,
                              val baseCurrencyAmountListener: BaseCurrencyAmountListener) : RecyclerView.Adapter<CurrencyRecyclerAdapter.CurrencyViewHolder>(){

    override fun getItemCount(): Int = currencyKeys.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CurrencyViewHolder {
        val view = parent.inflate(R.layout.currency_row)
        return CurrencyViewHolder(view)
    }

    override fun onBindViewHolder(holder: CurrencyViewHolder, position: Int) {
        val keys = currencyKeys[position]
        val values = currencyValues[position]
        holder.bindData(keys, values)
    }

    inner class CurrencyViewHolder(private val view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

        init {
            view.setOnClickListener(this)
        }

        fun bindData(key: String, value: Float){
            Picasso.get()
                .load(R.drawable.american_flag)
                .noFade()
                .into(view.flagIcon)
            view.currencyShort.text = key
            view.currencyFull.text = Currency.getInstance(key).displayName
            view.amount.setText("$value")

            view.amount.addTextChangedListener{
//                val amount = if (it.toString() == "") 0.0f else it.toString().toFloat()
                val amount = it.toString().toFloatOrNull()
                baseCurrencyAmountListener.onBaseAmountChange(key, amount)
            }
        }

        override fun onClick(v: View) {
            Log.d("RecyclerView", "clicked!")
        }

//        companion object {
//            private val CURRENCY_KEY = "CURRENCY"
//        }
    }
}