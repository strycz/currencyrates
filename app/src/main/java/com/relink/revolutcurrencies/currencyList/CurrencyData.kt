package com.relink.revolutcurrencies.currencyList

data class CurrencyData(var shortName: String, var longName: String, var rate: Float)