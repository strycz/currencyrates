package com.relink.revolutcurrencies.currencyList


interface BaseCurrencyAmountListener {
    fun onBaseAmountChange(base: String, amount: Float?)
}