package com.relink.revolutcurrencies.currencyRatesScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.relink.revolutcurrencies.rest.RatesRepository
import kotlinx.coroutines.Dispatchers


class CurrencyRatesViewModel : ViewModel(){

    private val repository = RatesRepository()

    val currencyLiveData = liveData(Dispatchers.IO) {
        val retrievedData = repository.getCurrencyRates("EUR")

        emit(retrievedData)
    }

}
