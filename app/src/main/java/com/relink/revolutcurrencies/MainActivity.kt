package com.relink.revolutcurrencies

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.relink.revolutcurrencies.currencyList.BaseCurrencyAmountListener
import com.relink.revolutcurrencies.currencyList.CurrencyRecyclerAdapter
import com.relink.revolutcurrencies.currencyRatesScreen.CurrencyRatesViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), BaseCurrencyAmountListener {

    private var rates: Map<String, Float>? = mapOf()
    private var calculatedRates: MutableMap<String, Float>? = mutableMapOf()
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var adapter: CurrencyRecyclerAdapter
    private lateinit var viewModel: CurrencyRatesViewModel

    private var adapterKeys = arrayListOf<String>()
    private var adapterValues = arrayListOf<Float>()

    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)


        linearLayoutManager = LinearLayoutManager(this)
        currencyRecyclerView.layoutManager = linearLayoutManager

        adapter = CurrencyRecyclerAdapter(adapterKeys, adapterValues, this)

        currencyRecyclerView.adapter = adapter

        viewModel = ViewModelProvider(this).get(CurrencyRatesViewModel::class.java)
        viewModel.currencyLiveData.observe(this, Observer {
            rates = it?.body()?.rates
            calculatedRates = rates?.toMutableMap()
            val base = it?.body()?.base
            val keysArray = ArrayList(rates?.keys)
            keysArray.add(0, base)
            val valuesArray = ArrayList(rates?.values)
            valuesArray.add(0, 0.0f)

            populateCurrencyInfo(keysArray, valuesArray)
        })
    }

    private fun populateCurrencyInfo(
        keysArray: ArrayList<String>,
        valuesArray: ArrayList<Float>
    ) {
        adapterKeys.clear()
        adapterKeys.addAll(keysArray)
        adapterValues.clear()
        adapterValues.addAll(valuesArray)
        currencyRecyclerView.adapter?.notifyDataSetChanged()
    }

    override fun onBaseAmountChange(base: String, amount: Float?) {
        println("PassedData: $base: $amount")
        if (amount != null) {
            rates?.forEach { (key, value) ->
                calculatedRates?.set(key, value * amount)
            }

            println("Calculated Rates: ")
            println("$calculatedRates")
        } else {
            calculatedRates?.clear()
        }
    }
}
