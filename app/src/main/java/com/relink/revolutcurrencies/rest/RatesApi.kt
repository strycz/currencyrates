package com.relink.revolutcurrencies.rest

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface RatesApi {
    @GET("/latest")
    suspend fun getRates(@Query(value = "base") baseCurrency: String): Response<CurrencyRatesObject>
}
