package com.relink.revolutcurrencies.rest

class RatesRepository {

    private var client = RetrofitFactory.ratesApi

    suspend fun getCurrencyRates(baseCurrency: String = "EUR") = client.getRates(baseCurrency)

}
